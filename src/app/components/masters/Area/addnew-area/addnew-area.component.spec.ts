import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddnewAreaComponent } from './addnew-area.component';

describe('AddnewAreaComponent', () => {
  let component: AddnewAreaComponent;
  let fixture: ComponentFixture<AddnewAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddnewAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddnewAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
