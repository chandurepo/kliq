import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HSNCreationComponent } from './hsn-creation.component';

describe('HSNCreationComponent', () => {
  let component: HSNCreationComponent;
  let fixture: ComponentFixture<HSNCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HSNCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HSNCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
