import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewHsnComponent } from './new-hsn.component';

describe('NewHsnComponent', () => {
  let component: NewHsnComponent;
  let fixture: ComponentFixture<NewHsnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewHsnComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewHsnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
