import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddnewBranchComponent } from './addnew-branch.component';

describe('AddnewBranchComponent', () => {
  let component: AddnewBranchComponent;
  let fixture: ComponentFixture<AddnewBranchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddnewBranchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddnewBranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
