import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewSalesmanComponent } from './new-salesman.component';

describe('NewSalesmanComponent', () => {
  let component: NewSalesmanComponent;
  let fixture: ComponentFixture<NewSalesmanComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewSalesmanComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewSalesmanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
