import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesmanCreationComponent } from './salesman-creation.component';

describe('SalesmanCreationComponent', () => {
  let component: SalesmanCreationComponent;
  let fixture: ComponentFixture<SalesmanCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SalesmanCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesmanCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
