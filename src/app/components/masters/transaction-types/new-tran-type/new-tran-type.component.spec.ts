import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTranTypeComponent } from './new-tran-type.component';

describe('NewTranTypeComponent', () => {
  let component: NewTranTypeComponent;
  let fixture: ComponentFixture<NewTranTypeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewTranTypeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTranTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
