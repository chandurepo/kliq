import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UOMCreationComponent } from './uom-creation.component';

describe('UOMCreationComponent', () => {
  let component: UOMCreationComponent;
  let fixture: ComponentFixture<UOMCreationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UOMCreationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UOMCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
