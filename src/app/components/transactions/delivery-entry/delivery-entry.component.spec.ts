import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryEntryComponent } from './delivery-entry.component';

describe('DeliveryEntryComponent', () => {
  let component: DeliveryEntryComponent;
  let fixture: ComponentFixture<DeliveryEntryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeliveryEntryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
