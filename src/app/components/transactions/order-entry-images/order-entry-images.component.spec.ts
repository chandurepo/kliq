import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderEntryImagesComponent } from './order-entry-images.component';

describe('OrderEntryImagesComponent', () => {
  let component: OrderEntryImagesComponent;
  let fixture: ComponentFixture<OrderEntryImagesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OrderEntryImagesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderEntryImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
