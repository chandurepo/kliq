import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-order-entry',
  templateUrl: './order-entry.component.html',
  styleUrls: ['./order-entry.component.scss']
})
export class OrderEntryComponent implements OnInit {
  isShown: boolean = false ;
  constructor() { }

  ngOnInit(): void {
  }
  sizeShow(){
    this.isShown = ! this.isShown;

  }
}
