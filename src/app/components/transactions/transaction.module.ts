import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeliveryEntryComponent } from './delivery-entry/delivery-entry.component';
import { OrderEntryComponent } from './order-entry/order-entry.component';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from '../masters/main/main.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { OrderEntryImagesComponent } from './order-entry-images/order-entry-images.component';
import { SalesComponent } from './sales/sales.component';
import { StockOutwardComponent } from './stock-outward/stock-outward.component';



const routes: Routes = [
  {
      path: '', component: MainComponent,
      children: [
          { path: 'orderEntry', component: OrderEntryComponent },
          { path: 'delivery-entry', component: DeliveryEntryComponent },
          { path: 'order-images', component: OrderEntryImagesComponent },
          { path: 'sales', component: SalesComponent },
          { path: 'stock-Outward', component: StockOutwardComponent },
          { path: '', redirectTo: 'dashboard', pathMatch: 'full' }
      ]
  }
];


@NgModule({
  declarations: [
    OrderEntryComponent,
    DeliveryEntryComponent,
    OrderEntryImagesComponent,
    SalesComponent,
    StockOutwardComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
   

  ]
})
export class TransactionModule { }
