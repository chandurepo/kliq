import { Component, Input, OnInit } from '@angular/core';
import * as sideMenu from './sidebar.config';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit {
  public isToggled: boolean = false;
  public menuOptions = sideMenu.SIDEBAR_MENU;
  constructor() {}

  ngOnInit(): void {}

  public toggleDropdown(e: any, menuObject: any = null) {
    e.stopPropagation();
    if (menuObject) menuObject.isClicked = !menuObject.isClicked;
  }

  public toggleMenu(e: any) {
    e.preventDefault();
    e.stopPropagation();
    document.querySelector('body')?.classList.toggle('ls-toggle-menu');
  }

  sidemenu() {}
}
