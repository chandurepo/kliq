export const SIDEBAR_MENU = [
  {
    menuText: 'Dashboard',
    icon: 'zmdi-home',
    routeLink: 'dashboard',
    extras: [],
    isClicked: false,
  },
  {
    menuText: 'Master',
    icon: 'zmdi-apps',
    routeLink: null,
    isClicked: false,
    extras: [
      {
        menuText: 'Branch',
        routeLink: 'createBranch',
        isClicked: false,
      },
      {
        menuText: 'Account Creation',
        routeLink: 'createAccount',
        isClicked: false,
      },
      {
        menuText: 'Create Size',
        routeLink: 'createSize',
        isClicked: false,
      },
      {
        menuText: 'salesman',
        routeLink: 'createSalesman',
        isClicked: false,
      },
      {
        menuText: 'Area',
        routeLink: 'createArea',
        isClicked: false,
      },
      {
        menuText: 'HSN Creation',
        routeLink: 'createHSN',
        isClicked: false,
      },
      {
        menuText: 'UOM',
        routeLink: 'createUOM',
        isClicked: false,
      },
      {
        menuText: 'Item Creation',
        routeLink: 'createItem',
        isClicked: false,
      },

      {
        menuText: 'Manufacture',
        routeLink: 'manufacture',
        isClicked: false,
      },
      {
        menuText: 'Shipping Creation',
        routeLink: 'shippingCreation',
        isClicked: false,
      },
      {
        menuText: 'ItemsCategory',
        routeLink: 'itemsCategory',
        isClicked: false,
      },
      {
        menuText: 'Charges',
        routeLink: 'charges',
        isClicked: false,
      },
      {
        menuText: 'Branch Bank',
        routeLink: 'Branch Bank',
        isClicked: false,
      },
      {
        menuText: 'Transaction Types',
        routeLink: 'transactionTypes',
        isClicked: false,
      },
    ],
  },
  {
    menuText: 'Transactions',
    icon: 'zmdi-assignment',
    routeLink: null,
    isClicked: false,
    extras: [
      {
        menuText: 'Order Entry',
        routeLink: '/orderEntry',
        isClicked: false,
      },
      {
        menuText: 'Delivery Entry',
        routeLink: '/delivery-entry',
        isClicked: false,
      },
      {
        menuText: 'Sales',
        routeLink: '/sales',
        isClicked: false,
      },
      {
        menuText: 'Stack outward',
        routeLink: '/stock-Outward',
        isClicked: false,
      },
    ],
  },
  {
    menuText: 'Stock Reports',
    icon: 'zmdi-folder',
    routeLink: null,
    isClicked: false,
    extras: [
      {
        menuText: 'Closing Stock analysis',
        routeLink: null,
        isClicked: false,
      },
    ],
  },
  {
    menuText: 'Registers',
    icon: 'zmdi-blogger',
    routeLink: null,
    isClicked: false,
    extras: [
      {
        menuText: 'sales Register',
        routeLink: null,
        isClicked: false,
      },
    ],
  },
  {
    menuText: 'GST Reports',
    icon: 'zmdi-shopping-cart',
    routeLink: null,
    isClicked: false,
    extras: [
      {
        menuText: 'Reports',
        routeLink: null,
        isClicked: false,
      },
    ],
  },
  {
    menuText: 'Margins',
    icon: 'zmdi-swap-alt',
    routeLink: null,
    isClicked: false,
    extras: [
      {
        menuText: 'item Wise Margin',
        routeLink: null,
        isClicked: false,
      },
    ],
  },
  {
    menuText: 'Stock Movement',
    icon: 'zmdi-flower',
    routeLink: null,
    isClicked: false,
    extras: [
      {
        menuText: 'Moving Stock',
        routeLink: null,
        isClicked: false,
      },
    ],
  },
  {
    menuText: 'Company',
    icon: 'zmdi-swap-alt',
    routeLink: null,
    isClicked: false,
    extras: [
      {
        menuText: 'About US',
        routeLink: null,
        isClicked: false,
      },
    ],
  },
];
